Source: osra
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders:
 Daniel Leidert <dleidert@debian.org>,
 Andrius Merkys <merkys@debian.org>,
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 libpgm2asc-dev,
 libgraphicsmagick++1-dev,
 libocrad-dev (>= 0.28-2),
 libopenbabel-dev,
 libpoppler-cpp-dev,
 libpotrace-dev,
 libtclap-dev,
Standards-Version: 4.6.1
Homepage: https://sourceforge.net/p/osra/wiki/Home/
Vcs-Browser: https://salsa.debian.org/debichem-team/osra
Vcs-Git: https://salsa.debian.org/debichem-team/osra.git

Package: osra
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Description: optical structure recognition
 OSRA is a utility designed to convert graphical representations of
 chemical structures, as they appear in journal articles, patent documents,
 textbooks, trade magazines etc., into SMILES (Simplified Molecular
 Input Line Entry Specification) - a computer recognizable molecular structure
 format. OSRA can read a document in any of the over 90 graphical formats
 parseable by ImageMagick - including GIF, JPEG, PNG, TIFF, PDF, PS etc., and
 generate the SMILES representation of the molecular structure images
 encountered within that document.

Package: libosra2
Architecture: any
Section: libs
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Description: optical structure recognition (shared library)
 OSRA is a utility designed to convert graphical representations of
 chemical structures, as they appear in journal articles, patent documents,
 textbooks, trade magazines etc., into SMILES (Simplified Molecular
 Input Line Entry Specification) - a computer recognizable molecular structure
 format. OSRA can read a document in any of the over 90 graphical formats
 parseable by ImageMagick - including GIF, JPEG, PNG, TIFF, PDF, PS etc., and
 generate the SMILES representation of the molecular structure images
 encountered within that document.

Package: libosra-dev
Architecture: any
Section: libdevel
Depends:
 libosra2 (= ${binary:Version}),
 ${shlibs:Depends},
 ${misc:Depends},
Description: optical structure recognition (development files)
 OSRA is a utility designed to convert graphical representations of
 chemical structures, as they appear in journal articles, patent documents,
 textbooks, trade magazines etc., into SMILES (Simplified Molecular
 Input Line Entry Specification) - a computer recognizable molecular structure
 format. OSRA can read a document in any of the over 90 graphical formats
 parseable by ImageMagick - including GIF, JPEG, PNG, TIFF, PDF, PS etc., and
 generate the SMILES representation of the molecular structure images
 encountered within that document.
